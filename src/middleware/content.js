import controllerFactory from '../controllers/content/controllerFactory';

export default ({dataBase}) => {
    return {
        validateUserBelongsToParking  : (req, res, next) => {
            const {body}              = req;
            const {parkingId, userId} = body;

            dataBase.parkings.findOne({
                where  : {uuid: parkingId},
                include: [{all: true, nested: true}]
            })
                .then(parking => {
                    const userBelongsToParking = parking.users.some(user => user.uuid === userId);

                    if (userBelongsToParking) {
                        req.parking = parking;
                        next();
                        return;
                    }

                    res.status(403).send({message: `User "${body.userId}" doesn't belong to parking "${body.parkingId}"`});
                })
                .catch((error) => {
                    console.log(error, 'ERRRRRRRRRO');
                    res.status(403).send({
                        message: `User "${body.userId}" doesn't belong to parking "${body.parkingId}"`
                    });
                });
        },
        validateContentBelongToParking: (req, res, next) => {
            const {params, parking} = req;
            const {type}            = params;

            controllerFactory({type, dataBase})
                .getById({params}, false)
                .then(entity => {
                    if (entity.parkingId === parking.id) {
                        req.entity = entity;
                        next();
                        return;
                    }

                    res.status(403).send({message: `This content doesn't belong to parking "${parking.id}"`});
                })
                .catch(() => {
                    res.status(403).send({message: `This content doesn't belong to parking "${parking.id}"`});
                });
        }
    };
};

