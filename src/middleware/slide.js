export default ({dataBase}) => {
    return {
        validateUserBelongsToParking: (req, res, next) => {
            const {body}              = req;
            const {parkingId, userId} = body;

            dataBase.parkings.findOne({
                where  : {uuid: parkingId},
                include: [{all: true, nested: true}]
            })
                .then(parking => {
                    const userBelongsToParking = parking.users.some(user => user.uuid === userId);

                    if (userBelongsToParking) {
                        req.parking = parking;
                        next();
                        return;
                    }

                    res.status(403).send({message: `User "${body.userId}" doesn't belong to parking "${body.parkingId}"`});
                })
                .catch(() => {
                    res.status(403).send({
                        message: `User "${body.userId}" doesn't belong to parking "${body.parkingId}"`
                    });
                });
        },
        validateSlideBelongToParking: (req, res, next) => {
            const {params, parking} = req;
            const {id}              = params;

            dataBase.slides.findOne({
                where: {id},
                include: [{all: true, nested: true}]
            })
                .then(slide => {
                    if (slide.parkingId === parking.id) {
                        req.slide = slide;
                        next();
                        return;
                    }

                    res.status(403).send({message: `This slide doesn't belong to parking "${parking.id}"`});
                })
                .catch(() => {
                    res.status(403).send({message: `This slide doesn't belong to parking "${parking.id}"`});
                });
        }
    };
};

