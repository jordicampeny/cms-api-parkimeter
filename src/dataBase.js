import { Sequelize } from 'sequelize';
import config from './config.json';
import synchronizeModels from './models';

export default callback => {
    const dataBase = new Sequelize(
        config.dataBase.dataBase,
        config.dataBase.user,
        config.dataBase.password, {
            host   : config.dataBase.host,
            dialect: config.dataBase.dialect,
        });

    dataBase.authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });

    const models = synchronizeModels({dataBase, config});

    callback(Object.assign(dataBase, models));
};
