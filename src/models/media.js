import { Sequelize } from 'sequelize';

export default (dataBase, parkings) => {
    const Media = dataBase.define('media', {
        url: {
            type: Sequelize.STRING,
            unique : true
        },
        type: {
            type: Sequelize.STRING
        },
        extension : {
            type: Sequelize.STRING
        }
    });

    parkings.hasMany(Media);

    parkings.sync()
        .then(() => {
            return Media.sync({force: false})
        })
        .then(() => {
            console.log('Media Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing Media Table', error);
        });

    return Media;
}
