import { Sequelize } from 'sequelize';

export default (dataBase, users) => {
    const Parking = dataBase.define('parking', {
        name        : {
            type     : Sequelize.STRING,
            allowNull: false,
        },
        parkimeterId: {
            type     : Sequelize.INTEGER,
            unique   : true,
            allowNull: false,
        },
        host        : {
            type     : Sequelize.STRING,
            allowNull: false,
            unique   : true
        },
        template    : {
            type: Sequelize.STRING
        },
        uuid        : {
            type        : Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique      : true
        },
        email       : {
            type    : Sequelize.STRING,
            validate: {
                isEmail: true
            }
        },
        phone       : {
            type: Sequelize.STRING
        },
        logo        : {
            type: Sequelize.STRING
        },
        cover       : {
            type: Sequelize.STRING
        }
    });

    const UserParkings = dataBase.define('userParkings', {
        userId   : {
            type: Sequelize.INTEGER
        },
        parkingId: {
            type: Sequelize.INTEGER
        }
    });

    users.belongsToMany(Parking, {through: UserParkings});
    Parking.belongsToMany(users, {through: UserParkings});

    Parking.sync({force: false})
        .then(() => {
            console.log('Parking Table synchronized.');
            return UserParkings.sync({force: false});
        })
        .then(() => {
            console.log('UserParkings Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing Parking Table', error);
        });

    return Parking;
};

