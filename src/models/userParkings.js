import { Sequelize } from 'sequelize';

export default (dataBase) => {
    const UserParkings = dataBase.define('userParkings', {
        userId: {
            type: Sequelize.INTEGER
        },
        parkingId: {
            type: Sequelize.INTEGER
        }
    });

    UserParkings.sync({force: false})
        .then(() => {
            console.log('UserParkings Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing UserParkings Table', error);
        });

    return UserParkings;
}
