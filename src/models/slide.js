import { Sequelize } from 'sequelize';

export default (dataBase, parkings, medias) => {
    const Slide = dataBase.define('slide', {
        name: {
            type: Sequelize.STRING
        },
        active : {
            type : Sequelize.BOOLEAN,
            defaultValue : false
        }
    });

    const MediaSlides = dataBase.define('mediaSlides', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    });

    medias.sync()
        .then(() => {
            return parkings.sync();
        })
        .then(() => {
            return medias.sync();
        })
        .then(() => {
            parkings.hasMany(Slide);

            return Slide.sync({force: false})
        })
        .then(() => {
            Slide.belongsToMany(medias, {as: 'Medias', through : MediaSlides, foreignKey : 'slideId'});
            medias.belongsToMany(Slide, {as: 'Slides', through : MediaSlides, foreignKey : 'mediaId'});

            return MediaSlides.sync({force: false})
        })
        .then(() => {
            console.log('Slide Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing Slide Table', error);
        });

    return Slide;
}
