import { Sequelize } from 'sequelize';

export default (dataBase) => {
    const User = dataBase.define('user', {
        email: {
            type: Sequelize.STRING,
            unique : true,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: Sequelize.STRING,
            defaultValue: null
        },
        firstName: {
            type: Sequelize.STRING,
        },
        lastName: {
            type: Sequelize.STRING,
        },
        role: {
            type:   Sequelize.ENUM,
            values: ['ROLE_ADMIN', 'ROLE_USER'],
            defaultValue: 'ROLE_USER'
        },
        uuid : {
            type : Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            unique : true
        }
    });

    User.sync({force: false})
        .then(() => {
            console.log('User Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing User Table', error);
        });

    return User;
}
