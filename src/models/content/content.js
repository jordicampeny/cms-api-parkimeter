export default (dataBase) => {
    const Content = dataBase.define('content');

    Content.sync({force: false})
        .then(() => {
            console.log('Content Table synchronized.');
        })
        .catch((error) => {
            console.log('Error synchronizing Content Table', error);
        });

    return Content;
};
