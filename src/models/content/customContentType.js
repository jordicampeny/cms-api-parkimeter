import { i18nFieldsGenerate } from '../../lib/content';
import { Sequelize } from 'sequelize';

function createCustomContentType ({dataBase, entityConfig, parkings}) {
    const CustomContentType = dataBase.define(entityConfig.type, Object.assign(
        i18nFieldsGenerate(entityConfig.i18nFields, Sequelize),
        entityConfig.genericAttributes,
        entityConfig.specificAttributes
    ));

    parkings.hasMany(CustomContentType);

    parkings.sync()
        .then(() => {
            return CustomContentType.sync({force: false})
        })
        .then(() => {
            console.log(`${entityConfig.type} Table synchronized.`);
        })
        .catch((error) => {
            console.log(`Error synchronizing ${entityConfig.type} Table`, error);
        });

    return CustomContentType;
}

export default (dataBase, parkings, entitiesConfigs) => {
    return entitiesConfigs.reduce((customContents, entityConfig) => {
        return Object.assign(customContents, {
            [`${entityConfig.type}s`]: createCustomContentType({dataBase, entityConfig, parkings})
        })
    }, {});
};