import { Sequelize } from 'sequelize';

export default (dataBase, Content, config) => {
    const Language = dataBase.define('language', {
        language: {
            type : Sequelize.STRING,
            unique : true
        }
    });

    const Translation = dataBase.define('translation', {
        text: Sequelize.TEXT
    });

    Language.sync({force: false})
        .then(() => {
            console.log('Language Table synchronized.');
            const languagesToCreate = config.languages.map(language => {
                return new Promise((resolve) => {
                    Language
                        .findOrCreate({where: {language}})
                        .spread((language, created) => {
                            if (created) {
                                console.log(language.get({plain: true}));
                            }
                            resolve(language);
                        })
                });
            });
            return Promise.all(languagesToCreate);
        })
        .then(() => {
            Language.hasMany(Translation);
            Translation.belongsTo(Language);
            Language.sync();

            Content.hasMany(Translation);
            Translation.belongsTo(Content);
            Content.sync();

            Translation.sync({force: false})
                .then(() => {
                    console.log('Translation Table synchronized.');
                })
                .catch((error) => {
                    console.log('Error synchronizing Translation Table', error);
                });
        })
        .catch((error) => {
            console.log('Error synchronizing Language Table', error);
        });

    return {translations : Object.assign(Translation, {languages : Language})};
};
