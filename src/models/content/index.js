import { Sequelize } from 'sequelize';
import createContents from './content';
import createTranslations from './translation';
import createCustomContents from './customContentType';

export const genericAttributes = {
    active : {
        type : Sequelize.BOOLEAN,
        defaultValue : false
    },
    order : {
        type : Sequelize.INTEGER,
        defaultValue: 0,
        allowNull : false
    }
};

export const coverConfig = {
    genericAttributes,
    type : 'cover',
    i18nFields: ['title', 'subTitle', 'description'],
    specificAttributes: {
        /* Other attributes here */
    },
    associations : []
};

export const offerConfig = {
    genericAttributes,
    type : 'offer',
    i18nFields: ['title', 'subTitle', 'description'],
    specificAttributes: {
        start: Sequelize.DATEONLY,
        end  : Sequelize.DATEONLY
    },
    associations : []
};

export const serviceConfig = {
    genericAttributes,
    type : 'service',
    i18nFields: ['title', 'description'],
    specificAttributes: {
        /* Other attributes here */
    },
    associations : []
};

export const locationConfig = {
    genericAttributes,
    type : 'location',
    i18nFields: ['title', 'description'],
    specificAttributes: {
        /* Other attributes here */
    },
    associations : []
};

export const alertConfig = {
    genericAttributes,
    type : 'alert',
    i18nFields: ['title', 'subTitle', 'description'],
    specificAttributes: {
        /* Other attributes here */
    },
    associations : []
};

export const entitiesConfigs = [
    coverConfig,
    serviceConfig,
    offerConfig,
    locationConfig,
    alertConfig
];

export default (dataBase, config, parkings) => {
    const Content     = createContents(dataBase);
    const Translation = createTranslations(dataBase, Content, config);
    const CustomContents = createCustomContents(dataBase, parkings, entitiesConfigs);

    return Object.assign(
        Content,
        CustomContents,
        Translation
    );
};