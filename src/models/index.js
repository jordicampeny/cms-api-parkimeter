import createUsers from './user';
import createParkings from './parking';
import createContents from './content';
import createMedias from './media';
import createSlides from './slide';


export default ({dataBase, config}) => {
    const users = createUsers(dataBase);
    const parkings = createParkings(dataBase, users);
    const medias = createMedias(dataBase, parkings);
    const slides = createSlides(dataBase, parkings, medias);
    const contents = createContents(dataBase, config, parkings);

    return {
        users,
        parkings,
        contents,
        medias,
        slides
    };
}
