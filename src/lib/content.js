import { Sequelize } from 'sequelize';

export function getEntityFromParkingModel ({parking, entityConfig, method = 'get'}) {
    const capitalizedType = `${entityConfig.type.charAt(0).toUpperCase()}${entityConfig.type.slice(1)}`;
    const contentMethod   = `${method}${capitalizedType}s`;

    return new Promise((resolve, reject) => {
        parking[contentMethod]()
            .then(entities => {
                resolve(entities);
            })
            .catch(error => {reject(error);});
    });
}

export function i18nFieldsGenerate (i18nFields, dataType) {
    return i18nFields.reduce((fields, fieldName) =>
            Object.assign(fields, {[fieldName]: dataType.INTEGER})
        , {});
}

export function geti18nFieldsFromBody (entityConfig, body) {
    const {i18nFields} = entityConfig;
    return i18nFields.reduce((validFields, i18nField) => {
        if (body[i18nField]) {
            Object.assign(validFields, {[i18nField]: body[i18nField]});
        }
        return validFields;
    }, {});
}

export function getSpecificFieldsFromBody (entityConfig, body) {
    const {specificAttributes} = entityConfig;
    return Object.keys(specificAttributes).reduce((validFields, specificField) => {
        if (body[specificField]) {
            Object.assign(validFields, {[specificField]: body[specificField]});
        }
        return validFields;
    }, {});
}

export function getGenericFieldsFromBody (entityConfig, body) {
    const {genericAttributes} = entityConfig;
    return Object.keys(genericAttributes).reduce((validFields, genericField) => {
        if (body[genericField]) {
            Object.assign(validFields, {[genericField]: body[genericField]});
        }
        return validFields;
    }, {});
}

export function buildTranslations ({languages, i18nBody, entity}) {
    const translations = [];

    languages.map(language => {
        Object.keys(i18nBody).map(i18nField => {
            const translation = {
                contentId : entity[i18nField],
                languageId: language.id,
                text      : i18nBody[i18nField],
            };
            translations.push(translation);
        });
    });

    return translations;
}

export function createTranslatedFields ({fields, dataBase, entity}) {
    const parsedFields = {};

    const translatedFields = fields.map(translationField => (
        new Promise((resolve, reject) => {
            dataBase.contents.create()
                .then(content => {
                    const field = {[translationField]: content.id};
                    Object.assign(parsedFields, field);
                    resolve(field);
                })
                .catch((error) => {reject(error);});
        })
    ));

    return new Promise((resolve, reject) => {
        Promise.all(translatedFields)
            .then(() => {
                entity.update(parsedFields)
                    .then(() => {resolve(Object.assign(entity, parsedFields));})
                    .catch((error) => {reject(error);});
            })
            .catch((error) => {reject(error);});
    });
}

export function createTranslations ({dataBase, entity, i18nBody, lang}) {
    const whereLang = (!lang || lang === 'all') ? ({}) : ({where: {language: lang}});

    return new Promise((resolve, reject) => {
        dataBase.contents.translations.languages.findAll(whereLang)
            .then(languages => {
                const translations        = buildTranslations({languages, i18nBody, entity});
                const translationsUpdates = translations.map(translation => {
                    return new Promise((translationResolve, translationReject) => {
                        dataBase.contents.translations.findOrCreate({
                            where   : {
                                languageId: translation.languageId,
                                contentId : translation.contentId
                            },
                            defaults: translation
                        }).spread((translationSaved, created) => {
                            if (!created) {
                                dataBase.contents.translations.update(translation, {
                                    where: {id: translationSaved.id}
                                })
                                    .then(() => {translationResolve();})
                                    .catch((error) => {translationReject(error);});
                                return;
                            }
                            resolve();
                        });
                    });
                });

                Promise.all(translationsUpdates)
                    .then(() => {
                        resolve();
                    })
                    .catch((error) => {reject(error);});

            })
            .catch((error) => {reject(error);});
    });
}

export function insertCustomContentType ({entityManager, dataBase, entityConfig, parking}) {
    return new Promise((resolve, reject) => {
        entityManager.create({
            parkingId: parking.id
        })
            .then(entity => {
                createTranslatedFields({
                    dataBase,
                    fields: entityConfig.i18nFields,
                    entity
                })
                    .then((entityNormalized) => {resolve(entityNormalized);})
                    .catch((error) => {reject(error);});
            })
            .catch((error) => {reject(error);});
    });
}

export function updateCustomContentType ({lang, body, dataBase, entityManager, entityConfig, id, entity}) {
    const i18nBody           = geti18nFieldsFromBody(entityConfig, body);
    const specificBody       = getSpecificFieldsFromBody(entityConfig, body);
    const genericBody        = getGenericFieldsFromBody(entityConfig, body);
    const attributesToUpdate = Object.assign(specificBody, genericBody);
    return createTranslations({dataBase, entity, i18nBody, lang})
        .then(() => {
            return entityManager.update(attributesToUpdate,
                {
                    where : {id},
                    fields: Object.keys(
                        Object.assign(entityConfig.specificAttributes, entityConfig.genericAttributes)
                    )
                });
        });
}

export function removeCustomContentType ({dataBase, entityManager, id, entity, entityConfig}) {
    const translationsIdToRemove = entityConfig.i18nFields.map(i18nField => ({contentId: entity[i18nField]}));
    const contentsIdToRemove     = entityConfig.i18nFields.map(i18nField => ({id: entity[i18nField]}));

    return new Promise((resolve, reject) => {
        entityManager.destroy({where: {id}})
            .then(() => {
                return dataBase.contents.translations.destroy({
                    where: {
                        $or: translationsIdToRemove
                    }
                });
            })
            .then(() => {
                return dataBase.contents.destroy({
                    where: {
                        $or: contentsIdToRemove
                    }
                });
            })
            .then(() => {
                resolve();
            })
            .catch((error) => reject(error));
    });
}

// from [entity.title = 1] to [entity.title : {en:{}, es:{}}]
export function includeContentsToEntities ({dataBase, entities, entityConfig, lang = 'all'}) {
    const contentIdsToFind = entities.reduce((contentsIds, entity) => {
        return contentsIds.concat(entityConfig.i18nFields.map(field => ({contentId: entity[field]})));
    }, []);
    const addLanguages  = (fieldTranslations) => {
        if (lang === 'all') {
            return fieldTranslations.reduce((fieldEntity, fieldTranslation) => {
                return Object.assign(fieldEntity, {[fieldTranslation.language.language]: fieldTranslation});
            }, {});
        }

        return fieldTranslations[0] || {};
    };
    const languagesToFind  = (lang === 'all') ? {} : {language: lang};

    return new Promise((resolve, reject) => {
        dataBase.contents.translations.findAll({
            where  : {
                $or: contentIdsToFind
            },
            include: [{
                model: dataBase.contents.translations.languages,
                where: {
                    id  : Sequelize.col('translation.id'),
                    $and: languagesToFind
                }
            }]
        })
            .then(translations => {
                const parsedEntities = entities.map(entity => {
                    entityConfig.i18nFields.map(field => {
                        const fieldTranslations = translations.filter(translation => translation.contentId === entity[field]);
                        entity[field]           = addLanguages(fieldTranslations);
                    });
                    return entity;
                });
                resolve(parsedEntities);
            })
            .catch(error => reject(error));
    });
}

export function getEntitiesOfTypeFromParking ({dataBase, entityConfig, parking, lang}, includeContent = true) {
    return new Promise((resolve, reject) => {
        getEntityFromParkingModel({parking, entityConfig, method: 'get'})
            .then(entities => {
                if (includeContent) {
                    return includeContentsToEntities({dataBase, entities, entityConfig, lang});
                }
                resolve(entities);
            })
            .then((entities) => {
                resolve(entities);
            })
            .catch(error => reject(error));
    });
}