export function parseIdParam (idParam = '') {
    if (typeof idParam !== 'string') {
        return '';
    }

    if (idParam.includes(',')) {
        const idRange = idParam.split(',').map(id => ({id}));
        return { $or : idRange};
    }

    return {id : idParam};
}

export function parseUuidParam (uuidParam = '') {
    if (typeof uuidParam !== 'string') {
        return '';
    }

    if (uuidParam.includes(',')) {
        const uuidRange = uuidParam.split(',').map(uuid => ({uuid}));
        return { $or : uuidRange};
    }

    return {uuid : uuidParam};
}

export function getValidAttributes (body = {}) {
    const validAttributes = {};

    Object.keys(body).forEach(attributeKey => {
        const attributeValue = body[attributeKey];
        if (typeof attributeValue !== 'undefined') {
            Object.assign(validAttributes, {[attributeKey] : attributeValue});
        }
    });

    return validAttributes;
}