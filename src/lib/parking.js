import config from '../config.json';

export function getLogo({parking}) {
    if (parking.logo) {
        return {
            url : `${config.filePaths.logos}/${parking.uuid}.${parking.logo}`
        };
    }
    return null;
}

export function getCover({parking}) {
    if (parking.cover) {
        return {
            url : `${config.filePaths.covers}/${parking.uuid}.${parking.cover}`
        };
    }
    return null;
}

export function parseMedia({media}) {
    if (!media.url || !media.extension){
        return null;
    }

    return Object.assign(media, {
        url : `${config.filePaths.media}/${media.url}.${media.extension}`
    });
}
