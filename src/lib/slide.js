export function setMediasToSlide ({dataBase, slides, slide}) {
    return new Promise((resolve, reject) => {
        if (Array.isArray(slides) && slides.length > 0) {
            const parsedSlidesId = slides.map(id => ({id}));
            dataBase.medias.findAll({
                where : {
                    $or : parsedSlidesId
                }
            })
                .then(medias => {
                    slide.setMedias(medias)
                        .then(() => {
                            resolve()
                        })
                        .catch(error => {reject(error)});
                })
                .catch(error => {reject(error)});
            return;
        }
        resolve()
    });
}
