import { Router } from 'express';

export default (controller) => {
    const usersApi = Router();

    /** Callback */
    usersApi.param('id', controller.onLoadParamId);

    /** GET /:id - Return given entity */
    usersApi.get('/:uuid', controller.getUserById);

    /** GET /:email - Return given entity */
    usersApi.post('/email', controller.getUserByEmail);

    /** GET / - Return all entities */
    usersApi.get('/', controller.getUsers);

    /** POST / - Create a new entity */
    usersApi.post('/', controller.createUser);

    /** POST / - Check user credentials */
    usersApi.post('/login', controller.loginUser);

    /** PUT /:id - Update a given entity */
    usersApi.put('/:id', controller.updateUser);

    /** DELETE /:id - Delete a given entity */
    usersApi.delete('/:id', controller.deleteUser);

    return usersApi;
};