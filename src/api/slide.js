import { Router } from 'express';

export default (controller, middleware) => {
    const slidesApi = Router();

    /** Callback */
    slidesApi.param('parkingId', controller.onLoadParkingId);

    /** GET /:parkingId - Return all entities */
    slidesApi.get('/:parkingId', controller.getSlides);

    /** GET /:parkingId/:id - Return given entity */
    slidesApi.get('/:parkingId/:id',
        middleware.validateSlideBelongToParking,
        controller.getSlideById
    );

    /** POST / - Create a new entity */
    slidesApi.post('/',
        middleware.validateUserBelongsToParking,
        controller.createSlide
    );

    /** PUT /:id - Update a given entity */
    slidesApi.put('/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateSlideBelongToParking,
        controller.updateSlide
    );

    /** DELETE /:id - Delete a given entity */
    slidesApi.delete('/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateSlideBelongToParking,
        controller.deleteSlide
    );

    return slidesApi;
};