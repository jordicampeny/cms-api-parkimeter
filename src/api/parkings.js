import { Router } from 'express';

export default (controller) => {
    const parkingsApi = Router();

    /** GET /<Array>:id - Return given entity */
    parkingsApi.get('/:id', controller.getParkingById);

    /** GET / - Return all entities */
    parkingsApi.get('/', controller.getParkings);

    /** POST / - Create a new entity */
    parkingsApi.post('/', controller.createParking);

    /** PUT /:id - Update a given entity */
    parkingsApi.put('/:id', controller.updateParking);

    /** DELETE /:id - Delete a given entity */
    parkingsApi.delete('/:id', controller.deleteParking);

    /** GET /validate/:host - Validate Parking */
    parkingsApi.get('/validate/:host', controller.validateParking);

    /** POST /:parkingId/user/<Array>:userId - Add User to Parking */
    parkingsApi.post('/:parkingId/user/:userId', controller.addUserToParking);

    /** DELETE /:parkingId/user/<Array>:userId - remove User from Parking */
    parkingsApi.delete('/:parkingId/user/:userId', controller.removeUserToParking);

    return parkingsApi;
};