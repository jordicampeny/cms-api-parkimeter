import { Router } from 'express';

export default (controller, middleware) => {
    const mediasApi = Router();

    /** Callback */
    mediasApi.param('parkingId', controller.onLoadParkingId);

    /** GET /:parkingId - Return all entities */
    mediasApi.get('/:parkingId', controller.getMedias);

    /** GET /:parkingId/:id - Return given entity */
    mediasApi.get('/:parkingId/:id',
        middleware.validateMediaBelongToParking,
        controller.getMediaById
    );

    /** POST / - Create a new entity */
    mediasApi.post('/',
        middleware.validateUserBelongsToParking,
        controller.createMedia
    );

    /** PUT /:id - Update a given entity */
    mediasApi.put('/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateMediaBelongToParking,
        controller.updateMedia
    );

    /** DELETE /:id - Delete a given entity */
    mediasApi.delete('/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateMediaBelongToParking,
        controller.deleteMedia
    );

    return mediasApi;
};