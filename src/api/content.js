import { Router } from 'express';

export default (controller, middleware) => {
    const contentsApi = Router();

    /** GET /:parkingId?lang="all/en/es/fr... ('all' default)" - Return all content from Parking */
    contentsApi.get('/:parkingId',
        controller.getContent
    );

    /** GET /:parkingId/:type - Return a type of content from Parking */
    contentsApi.get('/:parkingId/:type',
        controller.getContentByType
    );

    /** GET /:type/:id - Return a given content from Parking */
    contentsApi.get('/:parkingId/:type/:id',
        controller.getContentById
    );

    /** POST /:type - Create a type of content from Parking */
    contentsApi.post('/:type',
        middleware.validateUserBelongsToParking,
        controller.createContent
    );

    /** PUT /:type/:id - Update a content from Parking */
    contentsApi.put('/:type/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateContentBelongToParking,
        controller.updateContent
    );

    /** DELETE /:type/:id - Delete a content from Parking */
    contentsApi.delete('/:type/:id',
        middleware.validateUserBelongsToParking,
        middleware.validateContentBelongToParking,
        controller.removeContent
    );


    return contentsApi;
};
