import { Router } from 'express';

import usersApi from './users';
import mediaApi from './media';
import usersController from '../controllers/user';
import mediaController from '../controllers/media';

import parkingsApi from './parkings';
import parkingsController from '../controllers/parking';

import contentsApi from './content';
import contentsController from '../controllers/content/index';
import contentsMiddleware from '../middleware/content';

import mediasApi from './media';
import mediasController from '../controllers/media';
import mediasMiddleware from '../middleware/media';


import slidesApi from './slide';
import slidesController from '../controllers/slide';
import slidesMiddleware from '../middleware/slide';

export default ({dataBase}) => {
    const api = Router();

    api.use('/users', usersApi(
        usersController({
            dataBase
        })
    ));

    api.use('/parkings', parkingsApi(
        parkingsController({
            dataBase
        })
    ));

    api.use('/contents', contentsApi(
        contentsController({dataBase}),
        contentsMiddleware({dataBase})
    ));

    api.use('/medias', mediasApi(
        mediasController({dataBase}),
        mediasMiddleware({dataBase})
    ));

    api.use('/slides', slidesApi(
        slidesController({dataBase}),
        slidesMiddleware({dataBase})
    ));

    return api;
};
