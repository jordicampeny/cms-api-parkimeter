import {parseUuidParam, parseIdParam} from "../lib/util";
import {encrypt, decrypt} from "../lib/crypto";

export default ({dataBase}) => {
    return {
        onLoadParamId: (req, res, next, id) => {
            dataBase.users.findOne({where: parseUuidParam(id)})
                .then(user => {
                    req.user = user;
                    next();
                })
                .catch(error => res.status(404).send(error));
        },
        getUserById: ({user, params}, res) => {
            const {uuid} = params;
            dataBase.users.findAll({
                where: parseUuidParam(uuid),
                include: [{all: true, nested: true}]
            })
                .then(users => {
                    res.json(users);
                })
                .catch(error => res.status(404).send(error));
        },
        getUserByEmail: ({body}, res) => {
            let {email} = body;
            dataBase.users.findOne({
                where: {email},
            })
                .then(user => {
                    if(user && user.password && user.password !== null){
                        user.password = decrypt(user.password);
                        return res.json(user);
                    }
                    return res.status(403).send({error: "Unauthorized"});

                })
                .catch(error => res.status(404).send(error));

        },
        getUsers: (req, res) => {
            dataBase.users.findAll({
                include: [{all: true, nested: true}]
            })
                .then(users => {
                    res.json(users);
                })
                .catch(error => res.status(404).send(error));

        },
        createUser: ({body}, res) => {
            const {firstName, lastName, email, id} = body;
            dataBase.parkings.findOne({where: parseUuidParam(id)})
                .then(parking => {
                    dataBase.users.findOne({where: {email}})
                        .then(user => {
                            if (user) {
                                parking.addUsers(user)
                                    .then(() => {
                                        res.json(user);
                                    })
                                    .catch(error => res.status(400).send(error));
                            } else {
                                dataBase.users.create({firstName, lastName, email})
                                    .then(user => {
                                        //send email
                                        const nodemailer = require('nodemailer');
                                        let transporter = nodemailer.createTransport({
                                            host: 'email-smtp.eu-west-1.amazonaws.com',
                                            port: 587,
                                            secure: false, // secure:true for port 465, secure:false for port 587
                                            auth: {
                                                user: 'AKIAJKBQULB463QMVPEA',
                                                pass: 'As2Oh9MkSv2oQocWsPTr7+cPuWbaWWcH/xQiBF9iJHCb'
                                            }
                                        });
                                        let mailOptions = {
                                            from: '"Info Llingas" <info@llingas.net>',
                                            to: email,
                                            subject: 'Activate your CmsParking account',
                                            html: 'Welcome to CmsParking, You can activate your account by clicking on the following link: <a href="https://admin.cmsparking.net/activate-user/'+user.uuid+'">Activate account</a>',
                                            text: 'Welcome to CmsParking, You can activate your account by clicking on the following link: <a href="https://admin.cmsparking.net/activate-user/'+user.uuid+'">Activate account</a>'
                                        };
                                        transporter.sendMail(mailOptions, (error, info) => {
                                            if (error) {
                                                return console.log(error);
                                            }
                                            console.log('Message %s sent: %s', info.messageId, info.response);
                                        });
                                        console.log("SI SEND EMAIL", user.uuid);
                                        parking.addUsers(user)
                                            .then(() => {
                                                res.json(user);
                                            })
                                            .catch(error => res.status(404).send(error));
                                    })
                                    .catch(error => res.status(404).send(error));
                            }
                        })
                        .catch(error => res.status(404).send(error));
                })
                .catch(error => res.status(404).send(error));

        },
        loginUser: ({body}, res) => {
            let {email, password} = body;
            if(!password){
                return res.status(400).send('Password field is required.');
            }
            password = encrypt(password);
            dataBase.users.findOne({
                where: {email, password},
                include: [{all: true, nested: true}]
            })
                .then(user => {
                    if(user && user.password && user.password !== null){
                        return res.json(user);
                    }
                    return res.status(403).send({error: "Unauthorized"});

                })
                .catch(error => res.status(404).send(error));

        },
        updateUser: ({user, body}, res) => {
            let {password} = body;
            if (!password) {
                return res.status(400).send('Password field is required.');
            }
            if(!user){
                return res.status(400).send('User not exists.');
            }
            password = encrypt(password);
            dataBase.users.update({
                password
            }, {
                where: {
                    uuid: user.uuid
                }
            }).then(() => {
                res.sendStatus(204);
            }).catch(error => res.status(404).send(error));

        },
        deleteUser: ({user}, res) => {
            dataBase.users.destroy({
                where: {
                    uuid: user.uuid
                }
            }).then(() => {
                res.sendStatus(204);
            }).catch(error => res.status(404).send(error));
        }
    };
};