import { getValidAttributes } from '../lib/util';
import { setMediasToSlide } from '../lib/slide';
import { parseMedia } from '../lib/parking';

export default ({dataBase}) => {
    return {
        onLoadParkingId : (req, res, next, parkingId) => {
            dataBase.parkings.findOne({
                where  : {uuid : parkingId}
            })
                .then(parking => {
                    req.parking = parking;
                    next();
                })
                .catch(() => res.status(404).send(`The parking with id ${parkingId} doesn't exist`));
        },
        getSlides   : (req, res) => {
            const {parking} = req;

            dataBase.slides.findAll({
                where : {parkingId : parking.id},
                include: [{all: true, nested: true}]
            })
                .then(slides => {
                    if (Array.isArray(slides)) {
                        slides = slides.map(slide => {
                            slide.Medias = slide.Medias.map(media => parseMedia({media}));
                            return slide;
                        });
                        res.json(slides);
                        return;
                    }
                    res.status(404)
                })
                .catch(() => res.status(404).send(`Not found slides from parking with id ${parking.uuid}`));
        },
        getSlideById: (req, res) => {
            const {slide, parking} = req;

            if (slide.id) {
                slide.Medias = slide.Medias.map(media => parseMedia({media}));
                res.json(slide);
                return;
            }

            res.status(403).send({message: `This slide doesn't belong to parking "${parking.uuid}"`});
        },
        createSlide : (req, res) => {
            const {parking, body} = req;
            const {slides} = body;
            body.parkingId        = parking.id;

            dataBase.slides.create(getValidAttributes(body))
                .then(slide => {
                    setMediasToSlide({slides, dataBase, slide})
                        .then(() => {
                            res.json(slide);
                        })
                        .catch(error => {res.status(404).send(error)});
                })
                .catch(error => res.status(404).send(error));
        },
        updateSlide : (req, res) => {
            const {body, params, slide} = req;
            const {slides} = body;
            const {id} = params;
            delete body.parkingId;

            slide.update(
                getValidAttributes(body),
                {where: {id}}
            )
                .then(() => {
                    setMediasToSlide({slides, dataBase, slide})
                        .then(() => {
                            res.json(slide);
                        })
                        .catch(error => {res.status(404).send(error)});
                })
                .catch(error => res.status(404).send(error));
        },
        deleteSlide : (req, res) => {
            const {params} = req;
            const {id} = params;

            dataBase.slides.destroy({
                where: {id}
            }).then(() => {
                res.sendStatus(204);
            }).catch(error => res.status(404).send(error));
        }
    };
};