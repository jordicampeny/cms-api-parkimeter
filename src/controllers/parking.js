import { getValidAttributes, parseUuidParam } from '../lib/util';
import { getCover, getLogo, parseMedia } from '../lib/parking';

export default ({dataBase}) => {
    return {
        getParkingById     : ({params}, res) => {
            const {id} = params;

            dataBase.parkings.findOne({
                where  : parseUuidParam(id),
                include: [{all: true, nested: true}]
            })
                .then(parking => {
                    parking.logo = getLogo({parking});
                    parking.cover = getCover({parking});
                    parking.media = parking.media.map(media => parseMedia({media}));
                    res.json(parking);
                })
                .catch(error => res.status(404).send(error));
        },
        getParkings        : (req, res) => {
            dataBase.parkings.findAll({
                include: [{all: true, nested: true}]
            })
                .then(parkings => {
                    res.json(parkings);
                })
                .catch(error => res.status(404).send(error));

        },
        createParking      : ({body}, res) => {
            dataBase.parkings.create(
                getValidAttributes(body)
            )
                .then(parking => {
                    res.json(parking);
                })
                .catch(error => res.status(404).send(error));

        },
        updateParking      : ({body, params}, res) => {
            const {id} = params;

            dataBase.parkings.update(
                getValidAttributes(body),
                {where: {uuid: id}}
            )
                .then(() => {
                    res.sendStatus(204);
                })
                .catch(error => res.status(404).send(error));
        },
        deleteParking      : ({params}, res) => {
            const {id} = params;

            dataBase.parkings.destroy({
                where: {uuid : id}
            }).then(() => {
                res.sendStatus(204);
            }).catch(error => res.status(404).send(error));
        },
        validateParking    : ({params}, res) => {
            const {host} = params;

            dataBase.parkings.findOne({
                where: {host}
            }).then((parking) => {
                parking.logo = getLogo({parking});
                parking.cover = getCover({parking});
                res.json(parking);
            }).catch(error => {
                console.log(error);
                res.status(404).send(error)
            });
        },
        addUserToParking   : ({params}, res) => {
            const {parkingId, userId} = params;

            dataBase.parkings.findOne({
                where: {uuid: parkingId}
            })
                .then(parking => {
                    dataBase.users.findAll({
                        where: parseUuidParam(userId)
                    })
                        .then((users = []) => {
                            parking.addUsers(users)
                                .then(() => res.sendStatus(204))
                                .catch(error => res.status(404).send(error));
                        })
                        .catch(error => res.status(404).send(error));
                })
                .catch(error => res.status(404).send(error));
        },
        removeUserToParking: ({params}, res) => {
            const {parkingId, userId} = params;

            dataBase.parkings.findOne({
                where: {uuid: parkingId}
            })
                .then(parking => {
                    dataBase.users.findAll({
                        where: parseUuidParam(userId)
                    })
                        .then((users = []) => {
                            parking.removeUsers(users)
                                .then(() => res.sendStatus(204))
                                .catch(error => res.status(404).send(error));
                        })
                        .catch(error => res.status(404).send(error));
                })
                .catch(error => res.status(404).send(error));
        }
    };
};