import contentControllersFactory from './controllerFactory';
import { entitiesConfigs } from '../../models/content';
import { getEntitiesOfTypeFromParking } from '../../lib/content';

export default ({dataBase}) => {
    return {
        getContent      : ({params, query}, res) => {
            const {parkingId} = params;
            const {lang}      = query;

            dataBase.parkings.findOne({where: {uuid: parkingId}})
                .then((parking) => {
                    const entitiesParsed = entitiesConfigs.map(entityConfig => {
                        return getEntitiesOfTypeFromParking({dataBase, entityConfig, parking, lang});
                    });

                    Promise.all(entitiesParsed)
                        .then((entities) => {
                            const parsedResponse = entities.reduce((response, entity, index) => {
                                return Object.assign(response, {[`${entitiesConfigs[index].type}s`]: entity});
                            }, {});
                            res.json(parsedResponse);
                        })
                        .catch(error => {res.status(404).send(error);});
                })
                .catch(error => res.status(404).send(error));
        },
        getContentByType: (req, res) => {
            const {params, query} = req;
            const {type}          = params;
            const {lang}          = query;

            contentControllersFactory({type, dataBase})
                .getByType({params, lang})
                .then(content => {
                    res.json(content);
                })
                .catch(error => res.status(404).send(error));
        },
        getContentById  : (req, res) => {
            const {params, query} = req;
            const {type}          = params;
            const {lang}          = query;

            contentControllersFactory({type, dataBase})
                .getById({params, lang})
                .then(entity => {
                    res.json(entity);
                })
                .catch(error => {
                    console.log(error);
                    res.status(404).send(error);
                });
        },
        createContent   : (req, res) => {
            const {params} = req;
            const {type}   = params;

            contentControllersFactory({type, dataBase})
                .create(req)
                .then(content => {
                    res.json(content);
                })
                .catch(error => res.status(404).send(error));
        },
        updateContent   : (req, res) => {
            const {params, query} = req;
            const {type}          = params;
            const {lang = 'all'}  = query;

            contentControllersFactory({type, dataBase})
                .update(req, lang)
                .then(() => {
                    res.sendStatus(204);
                })
                .catch(error => res.status(404).send(error));
        },
        removeContent   : (req, res) => {
            const {params} = req;
            const {type}   = params;

            contentControllersFactory({type, dataBase})
                .remove(req)
                .then(() => {
                    res.sendStatus(204);
                })
                .catch(error => res.status(404).send(error));
        },
    };
};