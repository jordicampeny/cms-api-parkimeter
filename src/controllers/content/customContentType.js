import {
    insertCustomContentType, getEntitiesOfTypeFromParking, includeContentsToEntities,
    updateCustomContentType, removeCustomContentType
} from '../../lib/content';

export default ({dataBase, entityConfig, entityManager}) => {
    return {
        getByType: ({params, lang}, includeContent = true) => {
            const {parkingId} = params;

            return new Promise((resolve, reject) => {
                dataBase.parkings.findOne({where: {uuid: parkingId}})
                    .then((parking) => {
                        getEntitiesOfTypeFromParking({dataBase, entityConfig, parking, lang}, includeContent)
                            .then((entity) => {
                                resolve(entity);
                            })
                            .catch(error => reject(error));
                    })
                    .catch(error => reject(error));
            });
        },
        getById  : ({params, lang}, includeContent = true) => {
            const {id} = params;
            return new Promise((resolve, reject) => {
                entityManager.findOne({
                    where: {id}
                })
                    .then((entity) => {
                        if (!includeContent) {
                            resolve(entity);
                            return;
                        }
                        const entityWithContent = includeContentsToEntities({
                            entities: [entity],
                            dataBase,
                            entityConfig,
                            lang
                        });
                        resolve(entityWithContent);
                    })
                    .catch(error => reject(error));
            });
        },
        create   : ({parking}) => {
            return insertCustomContentType({
                parking,
                dataBase,
                entityManager,
                entityConfig
            });
        },
        update   : ({body, params, entity}, lang) => {
            return updateCustomContentType({
                entity,
                lang,
                body,
                dataBase,
                entityManager,
                entityConfig,
                id: params.id
            });
        },
        remove   : ({params, entity}) => {
            return removeCustomContentType({
                dataBase,
                entity,
                entityManager,
                entityConfig,
                id: params.id
            });
        }
    };
};

