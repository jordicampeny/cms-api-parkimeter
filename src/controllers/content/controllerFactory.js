import customContentTypeController from './customContentType';
import { entitiesConfigs } from '../../models/content/index';

export default ({type, dataBase}) => {
    const entityConfig = entitiesConfigs.filter(entitiesConfig => entitiesConfig.type === type)[0];

    if (entityConfig.type === type) {
        const entityManager = dataBase.contents[`${entityConfig.type}s`];

        return customContentTypeController({
            dataBase,
            entityManager,
            entityConfig
        });
    }

    throw `Type "${type}" doesn't exist`;
};
