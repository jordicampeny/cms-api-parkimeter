import { getValidAttributes } from '../lib/util';

export default ({dataBase}) => {
    return {
        onLoadParkingId : (req, res, next, parkingId) => {
            dataBase.parkings.findOne({
                where  : {uuid : parkingId}
            })
                .then(parking => {
                    req.parking = parking;
                    next();
                })
                .catch(() => res.status(404).send(`The parking with id ${parkingId} doesn't exist`));
        },
        getMedias   : (req, res) => {
            const {parking} = req;

            dataBase.medias.findAll({
                where : {parkingId : parking.id}
            })
                .then(medias => {
                    res.json(medias);
                })
                .catch(() => res.status(404).send(`Not found medias from parking with id ${parking.uuid}`));
        },
        getMediaById: (req, res) => {
            const {media, parking} = req;

            if (media.id) {
                res.json(media);
                return;
            }

            res.status(403).send({message: `This media doesn't belong to parking "${parking.uuid}"`});
        },
        createMedia : (req, res) => {
            const {parking, body} = req;
            body.parkingId        = parking.id;

            dataBase.medias.create(getValidAttributes(body))
                .then(media => {
                    res.json(media);
                })
                .catch(error => res.status(404).send(error));
        },
        updateMedia : (req, res) => {
            const {body, params} = req;
            const {id} = params;
            delete body.parkingId;

            dataBase.medias.update(
                getValidAttributes(body),
                {where: {id}}
            )
                .then(() => {
                    res.sendStatus(204);
                })
                .catch(error => res.status(404).send(error));
        },
        deleteMedia : (req, res) => {
            const {params} = req;
            const {id} = params;

            dataBase.medias.destroy({
                where: {id}
            }).then(() => {
                res.sendStatus(204);
            }).catch(error => res.status(404).send(error));
        }
    };
};