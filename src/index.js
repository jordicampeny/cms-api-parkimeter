import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './dataBase';
import middleware from './middleware';
import api from './api';
import config from './config.json';

const app = express();
app.server = http.createServer(app);

app.use(morgan('dev'));

app.use(cors({
	exposedHeaders: config.server.corsHeaders
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

initializeDb( dataBase => {

	app.use(middleware({ config, dataBase }));

	app.use('/api', api({ config, dataBase }));

	app.server.listen(process.env.PORT || config.server.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
